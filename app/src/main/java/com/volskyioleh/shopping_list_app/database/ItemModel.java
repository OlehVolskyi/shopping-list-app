package com.volskyioleh.shopping_list_app.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class ItemModel {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "item_text")
    private String itemText;

    @ColumnInfo(name = "item_status")
    private int itemStatus;

    @ColumnInfo(name = "item_date")
    private int itemDate;

    @ColumnInfo(name = "item_image")
    private String itemImage;

    public ItemModel(String itemText, int itemStatus, int itemDate, String itemImage){
        this.itemDate = itemDate;
        this.itemStatus = itemStatus;
        this.itemText = itemText;
        this.itemImage = itemImage;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemText() {
        return itemText;
    }

    public void setItemText(String itemText) {
        this.itemText = itemText;
    }


    public int getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(int itemStatus) {
        this.itemStatus = itemStatus;
    }

    public int getItemDate() {
        return itemDate;
    }

    public void setItemDate(int itemDate) {
        this.itemDate = itemDate;
    }

    public String getItemImage() {
        return itemImage;
    }

    public void setItemImage(String itemImage) {
        this.itemImage = itemImage;
    }
}
