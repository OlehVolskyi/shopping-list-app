package com.volskyioleh.shopping_list_app.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = ItemModel.class, version = 1,exportSchema = false)
public abstract class ItemsDatabase extends RoomDatabase {

    private static ItemsDatabase mInstance;

    public static ItemsDatabase getDatabase(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context.getApplicationContext(), ItemsDatabase.class, "items_db")
                    .build();
        }
        return mInstance;
    }

    public static void destroyInstances(){
        mInstance = null;
    }

    public abstract ItemDao itemModelDao();
}
