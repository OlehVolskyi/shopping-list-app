package com.volskyioleh.shopping_list_app.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;
@Dao
public interface ItemDao {

    @Query("SELECT id,item_text,item_status,item_date,item_image FROM itemmodel WHERE item_status=0")
    LiveData<List<ItemModel>> getNoPurchaseItems();

    @Query("SELECT id,item_text,item_status,item_date,item_image FROM itemmodel WHERE item_status=1")
    LiveData<List<ItemModel>> getPurchaseItems();


    @Query("UPDATE itemmodel SET item_status=:status WHERE id = :id")
    void update(int status, int id);

    @Insert(onConflict = REPLACE)
    void insertAll(ItemModel itemModels);

}
