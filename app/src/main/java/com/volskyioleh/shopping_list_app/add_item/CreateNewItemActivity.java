package com.volskyioleh.shopping_list_app.add_item;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.volskyioleh.shopping_list_app.R;
import com.volskyioleh.shopping_list_app.database.ItemModel;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateNewItemActivity extends AppCompatActivity {


    @BindView(R.id.add_photo_from_gallery_btn)
    ImageView imageFromGallery;
    @BindView(R.id.add_photo_from_camera_btn)
    ImageView imageFromCamera;
    @BindView(R.id.addedImageView)
    ImageView newImage;
    @BindView(R.id.edit_text)
    EditText editItemText;

    private AddItemViewModel mViewModel;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    static final int REQUEST_TAKE_PHOTO = 1;
    private String mCurrentPhotoPath;
    private boolean permissionKey = false;
    static final int REQUEST_IMAGE_CAPTURE = 1;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_item);

        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mViewModel = new AddItemViewModel(getApplication());
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//        }
//        isStoragePermissionGranted();
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Toast.makeText(this, "Create file failed", Toast.LENGTH_SHORT).show();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);


            }
        }
    }

    @OnClick(R.id.add_photo_from_gallery_btn)
    public void galleryCLick() {
        permissionKey = true;
        if (isStoragePermissionGranted()) {
            storageIntent();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick(R.id.add_photo_from_camera_btn)
    public void cameraCLick() {
        permissionKey = false;
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
        } else {
            dispatchTakePictureIntent();
        }
    }


    @OnClick(R.id.fab)
    public void fabClick() {
        if (isEmpty(editItemText.getText().toString()) && mCurrentPhotoPath == null) {
            Toast.makeText(this, "Fill in the field or add an image", Toast.LENGTH_SHORT).show();
        } else if (mCurrentPhotoPath != null && editItemText.getText().toString().trim().isEmpty()) {
            Calendar calendar = Calendar.getInstance();
            int date = (int) calendar.getTimeInMillis();
            mViewModel.addItem(new ItemModel(" ", 0, date, mCurrentPhotoPath));
            finish();
        } else {
            Calendar calendar = Calendar.getInstance();
            int date = (int) calendar.getTimeInMillis();
            mViewModel.addItem(new ItemModel(editItemText.getText().toString(), 0, date, mCurrentPhotoPath));
            finish();
        }
    }


    private boolean isEmpty(String itemText) {
        if (TextUtils.isEmpty(itemText)) {
            return true;
        }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_CAMERA_PERMISSION_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isStoragePermissionGranted()) {
                        dispatchTakePictureIntent();
                    }
                }
                return;
            }
            case 1232: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("INTENT", "YEEEEEW");
                    if (permissionKey) {
                        storageIntent();
                    } else {
                        dispatchTakePictureIntent();
                    }
                }
            }
        }
    }

    private void storageIntent() {
        if (isStoragePermissionGranted()) {
            Intent intent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
            final int ACTIVITY_SELECT_IMAGE = 1234;
            startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
        }
    }

    public boolean isStoragePermissionGranted() {
        String TAG = "PERMISSIONS";
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Log.v(TAG, "Permission is granted");
                return true;
            } else {
                Log.v(TAG, "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1232);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v(TAG, "Permission is granted");
            return true;
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("PACH CAMERA", mCurrentPhotoPath);
        return image;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Picasso.get().load(new File(mCurrentPhotoPath)).into(newImage);
        }


        switch (requestCode) {
            case 1234:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = null;
                    if (selectedImage != null) {
                        cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    }

                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String filePath = cursor.getString(columnIndex);
                    cursor.close();
                    mCurrentPhotoPath = filePath;
                    Picasso.get().load(new File(filePath)).into(newImage);
                }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
