package com.volskyioleh.shopping_list_app.add_item;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.volskyioleh.shopping_list_app.database.ItemModel;
import com.volskyioleh.shopping_list_app.database.ItemsDatabase;

public class AddItemViewModel extends AndroidViewModel {

    private ItemsDatabase mItemsDatabase;

    public AddItemViewModel(@NonNull Application application) {
        super(application);

        mItemsDatabase = ItemsDatabase.getDatabase(getApplication());
    }


    public void addItem(final ItemModel itemModel){
        new addAsyncTask(mItemsDatabase).execute(itemModel);
    }


    private class addAsyncTask extends AsyncTask<ItemModel,Void,Void> {

        private ItemsDatabase db;

        public addAsyncTask(ItemsDatabase mItemsDatabase) {
            db = mItemsDatabase;
        }

        @Override
        protected Void doInBackground(final ItemModel... itemModels) {
            db.itemModelDao().insertAll(itemModels[0]);
            return null;
        }
    }
}
