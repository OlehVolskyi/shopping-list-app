package com.volskyioleh.shopping_list_app.add_item;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.volskyioleh.shopping_list_app.database.ItemModel;
import com.volskyioleh.shopping_list_app.database.ItemsDatabase;

public class UpdateStatusViewModel extends AndroidViewModel {

    private ItemsDatabase mItemsDatabase;

    public UpdateStatusViewModel(@NonNull Application application) {
        super(application);


        mItemsDatabase = ItemsDatabase.getDatabase(getApplication());
    }

    public void updateItem(final ItemModel itemModel){
        new UpdateStatusViewModel.UpdateAsyncTask(mItemsDatabase).execute(itemModel);
    }

    private class UpdateAsyncTask extends AsyncTask<ItemModel,Void,Void> {

        private ItemsDatabase db;

        UpdateAsyncTask(ItemsDatabase mItemsDatabase) {
            db = mItemsDatabase;
        }


        @Override
        protected Void doInBackground(ItemModel... lists) {
            db.itemModelDao().update(lists[0].getItemStatus(),lists[0].getId());
            return null;
        }
    }


}
