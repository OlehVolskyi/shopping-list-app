package com.volskyioleh.shopping_list_app;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.volskyioleh.shopping_list_app.add_item.CreateNewItemActivity;
import com.volskyioleh.shopping_list_app.add_item.UpdateStatusViewModel;
import com.volskyioleh.shopping_list_app.database.ItemModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements ItemAdapter.AdapterCallback {

    @BindView(R.id.fab)
    FloatingActionButton addNewItemBtn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private Snackbar mSnackbar;
    private ItemAdapter mAdapter;
    private List<ItemModel> checkedItems;


    private UpdateStatusViewModel mUpdateStatusViewModel;
    private CheckBox checkBox;
    private boolean checkKey = false;
    private boolean unCheckKey = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        checkedItems = new ArrayList<>();


        mAdapter = new ItemAdapter(new ArrayList<ItemModel>(), this, false);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(null);
        ItemViewModel mItemViewModel = ViewModelProviders.of(this).get(ItemViewModel.class);
        mItemViewModel.getItemsList().observe(MainActivity.this, new Observer<List<ItemModel>>() {
            @Override
            public void onChanged(@Nullable List<ItemModel> itemModels) {
                mAdapter.addItem(itemModels);
            }
        });
        mUpdateStatusViewModel = new UpdateStatusViewModel(getApplication());

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.history:
                startActivity(new Intent(MainActivity.this, HistoryActivity.class));
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        checkBox = (CheckBox) menu.findItem(R.id.menuShowDue).getActionView();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    checkKey = false;
                    unCheckKey = true;
                    mAdapter.updateToAllUnChek();
                } else {
                    mAdapter.updateToAllCheck();
                    checkKey = true;
                    checkedItems = mAdapter.getItems();
                    snackbarShow();
                }
            }
        });
        return true;
    }


    View.OnClickListener snackbarOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mSnackbar.dismiss();
            addNewItemBtn.show();
            updateStatus();
            //  mAdapter.notifyDataSetChanged();
            checkBox.postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkBox.setChecked(false);
                }
            }, 100);
        }
    };


    @OnClick(R.id.fab)
    public void fabClick() {
        startActivity(new Intent(MainActivity.this, CreateNewItemActivity.class));
    }


    @Override
    public void checkClick(int position, ItemModel model, boolean isChecked) {

        int id = model.getId();

        if (isChecked) {
            if (!checkKey) {
                model.setItemStatus(1);
                snackbarShow();
                checkedItems.add(model);
            }
        } else {
            if (!unCheckKey) {
                for (int i = 0; i < checkedItems.size(); i++) {
                    if (checkedItems.get(i).getId() == id) {
                        checkedItems.remove(checkedItems.get(i));
                    }
                }
            } else {
                snackbarHide();
                unCheckKey = false;
            }
        }
        if (checkedItems.size() <= 0) {
            snackbarHide();
        }
    }


    private void updateStatus() {
        for (int i = 0; i < checkedItems.size(); i++) {
            mUpdateStatusViewModel.updateItem(checkedItems.get(i));

        }
    }

    private void snackbarHide() {
        addNewItemBtn.show();
        mSnackbar.dismiss();
    }

    private void snackbarShow() {
        if (mSnackbar == null) {
            mSnackbar = Snackbar.make(addNewItemBtn, "Add to purchased", Snackbar.LENGTH_INDEFINITE)
                    .setAction("Done", snackbarOnClickListener);
        }
        if (!mSnackbar.isShown()) {
            mSnackbar.show();
            addNewItemBtn.hide();
        }
    }

}
