package com.volskyioleh.shopping_list_app;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.volskyioleh.shopping_list_app.database.ItemModel;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {
    private List<ItemModel> mItems;
    private AdapterCallback mAdapterCallback;
    private boolean mIsHistoryKey;

    ItemAdapter(List<ItemModel> items, Context context, boolean historyKey) {
        mItems = items;
        this.mAdapterCallback = ((AdapterCallback) context);
        mIsHistoryKey = historyKey;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ItemViewHolder(view);
    }


    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ItemAdapter.ItemViewHolder holder, final int position) {

        final ItemModel itemModel = mItems.get(position);

        if (!mIsHistoryKey) {
            if (itemModel.getItemStatus() == 1) {
                holder.checkBox.setChecked(true);
            } else {
               holder.checkBox.setChecked(false);
            }

            holder.checkBox.setOnCheckedChangeListener(
                    new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            mAdapterCallback.checkClick(position, itemModel, isChecked);
                        }
                    }
            );
        } else {
            holder.checkBox.setVisibility(View.INVISIBLE);
        }

        if (itemModel.getItemImage() != null) {
            holder.imageItem.getLayoutParams().height = 500;
            holder.imageItem.setVisibility(View.VISIBLE);
            setText(holder, true, itemModel, position);
            Picasso.get().load(new File(itemModel.getItemImage())).config(Bitmap.Config.RGB_565).into(holder.imageItem);
        } else {
            holder.imageItem.setVisibility(View.GONE);
            setText(holder, false, itemModel, position);
        }

    }

    @SuppressLint("ResourceAsColor")
    private void setText(final ItemViewHolder holder, final Boolean isImage, final ItemModel itemModel, int position) {

        if (isImage) {
            if (itemModel.getItemText().trim().isEmpty()) {
                holder.itemText.setVisibility(View.INVISIBLE);
            } else {
                holder.itemText.setVisibility(View.VISIBLE);
                holder.itemText.setText(itemModel.getItemText());
            }
            if (position == holder.getAdapterPosition()) {
                holder.itemText.setBackgroundColor(Color.parseColor("#77000000"));
                holder.itemText.setTextColor(Color.WHITE);
            }
        } else {
            holder.itemText.setText(itemModel.getItemText());
            holder.itemText.setVisibility(View.VISIBLE);
            if (position == holder.getAdapterPosition()) {
                holder.itemText.setBackgroundColor(Color.TRANSPARENT);
                holder.itemText.setTextColor(R.color.colorTrans);
            }
        }
    }


    public List<ItemModel> getItems() {
        return mItems;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public void updateToAllCheck() {
        int status = 1;

        for (int i = 0; i < mItems.size(); i++) {
            mItems.get(i).setItemStatus(status);
        }

        notifyDataSetChanged();
    }

    public void updateToAllUnChek() {
        int status = 0;

        for (int i = 0; i < mItems.size(); i++) {
            mItems.get(i).setItemStatus(status);
        }

        notifyDataSetChanged();


    }

    public void addItem(List<ItemModel> itemModels) {
        mItems = itemModels;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemTv)
        TextView itemText;
        @BindView(R.id.imageItem)
        ImageView imageItem;
        @BindView(R.id.isBy_ChBx)
        CheckBox checkBox;

        ItemViewHolder(View itemView) {
            super(itemView);
            // itemText = itemView.findViewById(R.id.itemTv);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface AdapterCallback {
        void checkClick(int position, ItemModel model, boolean isChecked);
    }
}
