package com.volskyioleh.shopping_list_app;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.volskyioleh.shopping_list_app.database.ItemModel;
import com.volskyioleh.shopping_list_app.database.ItemsDatabase;

import java.util.List;

public class ItemViewModel extends AndroidViewModel {

    private final LiveData<List<ItemModel>> mItemsList;

    public ItemViewModel(@NonNull Application application) {
        super(application);

        ItemsDatabase mAppDatabase = ItemsDatabase.getDatabase(this.getApplication());
        mItemsList = mAppDatabase.itemModelDao().getNoPurchaseItems();
    }

    public LiveData<List<ItemModel>> getItemsList() {
        return mItemsList;
    }
}
